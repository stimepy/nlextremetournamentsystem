Welcome to NukeLadder Extreme Tournament System (2.6.4) for E107!


BUG REPORTS:
First: http://sourceforge.net/tracker/?func=add&group_id=293105&atid=1238682
Second: http://www.aodhome.com
or
Email: stimepy@aodhome.com



Whats New
-------------
see changelog


Notice:
-------------
Things are still underdevelopment but 99% of this should work.  Any Bugs or problems please report them to: http://sourceforge.net/tracker/?func=add&group_id=293105&atid=1238682


Know Problems:
---------------
None that I can think of, please let me know through a bug report if you find ANY problems.



New Installation
-----------------
(Note* If you have run Nukeladder previously and want to start fresh, please uninstall as appropriate and remove any Nukeladder related files.)

-Unzip the the package and locate the 'html' folder.
-Upload the contents of the 'html' folder to your root system directory.
-Login to the admin area of your site and go to the Plugin Manager
-Locate the Extreme Tournament System plugin in the manager and click install
- Make sure to go to the Menus area of the admin section find XTS menu and activate it.


Upgrade Installation from Nukeladder 2.5.5x or 2.6.x
-----------------
Nope, not at this time.