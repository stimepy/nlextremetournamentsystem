<?php
###############################################################
##X1plugin Competition Management
##Homepage::http://www.aodhome.com
##Copyright:: Kris Sherrerd 2008-2011
##Version 2.6.4
###############################################################
// Change, add, modify as you write out what words you are going to use.  These are just some basic ones.
define('X2_vs','vs ');
define('X2_drawvs', 'Draw vs ');
define('X2_earlychal','You have played this team with in the last 2 days please wait a few days to challenge this team again');
define('X2_team','Team:');
define('X2_status','Status:');
define('X2_leaderboard', 'Leaderboard  ::  ');
define('X2_wins','Wins');
define('X2_losses','Losses');
define('X2_streak','Streak');
define('X2_challengedby', 'Challenged by ');
define('X2_challenged', 'Challenged ');
define('X2_drawvs', "Draw vs ");
define('yourmod_followup', '<br /><br />Followup::');
define('X2_openchall', 'Open for challenge');
define('X2_modinfotitle', 'Ladder Competition');
define('X2_modinfodesc', 'Info File Contains Information about the mod, author, settings,  
			this is public and should be treated as such.
			The Ladder competition is based on rung position. 
			Opponnents fight for position and can only challenge above thier position.');
define('X2_noteams','No teams have joined this event yet.');
define('X2_aevents_lex1', 'Rating: (0 or more points)');
?>
