<?php
###########################################
# SCRIPT CONFIG FOR PHP-Nuke
# systemconfig.php
###########################################
# ERROR REPORTING OPTIONS
#error_reporting(2047);
###########################################
# PATH OPTIONS
# Path options define directorys where files should exsist and what to insert 
# into certain links to trigger certain actions within a cms.
###########################################

define('X1_plugpath', "modules/extremetournamentsystem/");
#Path to css directory
define('X1_csspath', X1_plugpath."css");
#Path to images directory
define('X1_imgpath', X1_plugpath."images");
#Path to javascripts
define('X1_jspath', X1_plugpath."jscript");
#Path to plugin mod files
define('X1_modpath',X1_plugpath."mods");
#Path to language files
define('X1_langpath', X1_plugpath."language");
#Path to email files
define('X1_emailpath', X1_plugpath."templates/emails");
#Default Logo
define('X1_team_image', X1_imgpath.'/deflogo.gif');
#default log path
define('X1_logpath', X1_plugpath.'logs');

#File to use in POST requests in admin
define('X1_adminpostfile', 'modules.php?name=extremetournamentsystem');
#File to use in GET requests in admin
define('X1_admingetfile', 'modules.php');

#File to use in POST requests in core
define('X1_publicpostfile', 'modules.php?name=extremetournamentsystem');
#File to use in GET requests in core
define('X1_publicgetfile', 'modules.php');
#Action operators
define('X1_linkactionoperator', '&op?=');
define('X1_actionoperator', 'op');
define('X1_urlx_path','');

#Which cms the plugin is running in
define('X1_parent', 'raven');
#Output format of the plugin
define('X1_output', "echo");
#configuration console
define('X1_useconfigpanel',true);

define('X1_admin_log','admin');
define('X1_user_log', 'user');

###########################################
# DATABASE MAPPING OPTIONS
# The following tables define which prefixes and which database tables to use.
# If you have a default setup, most of these should remain as is.
# Some nuke users may need to change the prefix options
###########################################
#Use external adodb abstraction layer, NukeEvo 2.0.0+ set to false as it has built-in adodb
define('X1_useadodblite', true);

#main tables prefix
define('X1_prefix', 'nuke_xts_'); 
#user table prefix
define('X1_userprefix', 'nuke_');

#Users Main
#CMS Database table containing users
define('X1_DB_userstable', 'users');
#Key name which contains user's id
define('X1_DB_usersidkey', 'user_id');
#Key name which contains user's name
define('X1_DB_usersnamekey', 'username');
#Key name which contains user's email
define('X1_DB_usersemailkey', 'user_email');
#Key name which contains user's fake email
define('X1_DB_usersfakeemailkey', 'femail');
#Key name which contains user's public email flag
define('X1_DB_usersviewemailkey', 'user_viewemail');


?>