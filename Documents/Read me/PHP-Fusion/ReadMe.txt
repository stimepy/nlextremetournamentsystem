Welcome to NukeLadder Extreme Tournament System (2.6.4) for PHP-Fusion!


BUG REPORTS:
First: http://sourceforge.net/tracker/?func=add&group_id=293105&atid=1238682
Second: http://www.aodhome.com
or
Email: stimepy@aodhome.com



Whats New
-------------
see changelog


Notice:
-------------
Things are still underdevelopment but 99% of this should work.  Any Bugs or problems please report them to: http://sourceforge.net/tracker/?func=add&group_id=293105&atid=1238682


Know Problems:
---------------
I know that there are some notices that are showing up, these are usually nothing to worry about but please report them as you find them.



New Installation
-----------------
(Note* If you have run Nukeladder previously and want to start fresh, please uninstall as appropriate and remove any Nukeladder related files.)

-Unzip the the package and locate the 'html' folder.
-Upload the contents of the 'html' folder to your root system directory.
-Login to the admin area of your site and go to infusions, then infuse the module.
-To activate the main menu goto the panel section and add a new panel selecting the xts_panel.  Make sure the enable it after you have added it.


Upgrade Installation from Nukeladder 2.5.5x or 2.6.x
-----------------
Nope, not at this time.