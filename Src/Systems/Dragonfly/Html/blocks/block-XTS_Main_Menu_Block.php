<?php

/**
* CPG Dragonfly CMS
* Copyright © 2004 - 2006 by CPG-Nuke Dev Team, dragonflycms.org
* Released under the GNU GPL version 2 or any later version
* $Id: block-Sample.php,v 1.4 2006/10/07 07:54:11 nanocaiordo Exp $
*/

// protect against direct access
if (!defined('CPG_NUKE')) { exit; }

// show the content of the block
$module_name="extremetournamentsystem";
if(is_admin($admin))$adminlink = "<strong><big>&middot;</big></strong>&nbsp;<a href=\"$module_name/op=admin/\">Admin Panel</a>";
$content =  "
<strong><big>&middot;</big></strong>&nbsp;<a href=\"$module_name/op=home/\">Events</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"$module_name/op=myteams/\">Team Administration</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"$module_name/op=createteam/\">CreateTeam</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"$module_name/op=jointeamform/\">Join Team</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"$module_name/op=quitteamform/\">Quit Team</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"$module_name/op=teamlist/\">Team List</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"/$module_name/op=modindex/\">Moderator Area</a><br />
$adminlink";
?>
