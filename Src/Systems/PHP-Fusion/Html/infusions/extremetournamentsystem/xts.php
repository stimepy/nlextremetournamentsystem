<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright � 2002 - 2008 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: new_infusion.php
| Author: INSERT NAME HERE
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
require_once "../../maincore.php";
require_once THEMES."templates/header.php";
//include INFUSIONS."extremetournamentsystem/infusion_db.php";

// Check if locale file is available matching the current site locale setting.
if (file_exists(INFUSIONS."extremetournamentsystem/locale/".$settings['locale'].".php")) {
	// Load the locale file matching the current site locale setting.
	include INFUSIONS."extremetournamentsystem/locale/".$settings['locale'].".php";
} else {
	// Load the infusion's default locale file.
	include INFUSIONS."extremetournamentsystem/locale/English.php";
}
//start XTS code
	define('X1plugin_include', true);

	# Load X1 Config
	require_once("includes/X1File.class.php");

	opentable("");//Header for Fusion
	define('parent_path', '');
	 
		 	X1File::X1LoadFile("nukeladdersystem.php");
	
closetable();//Footer for fusion

require_once THEMES."templates/footer.php";
?>