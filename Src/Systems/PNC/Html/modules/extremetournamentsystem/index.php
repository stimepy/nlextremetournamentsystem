<?php
###############################################################
##Nuke Ladder - XTS
##Homepage::http://www.aodhome.com
##Copyright:: Shane Andrusiak 2000-2006   Kris Sherrerd 2008-2010 (2.6.0)
##Version 2.6.4
###############################################################
## +-----------------------------------------------------------------------+
## | This file is free software; you can redistribute it and/or modify     |
## | it under the terms of the GNU General Public License as published by  |
## | the Free Software Foundation; either version 3 of the License.        |                           |
## | This file is distributed in the hope that it will be useful           |
## | but WITHOUT ANY WARRANTY; without even the implied warranty of        |
## | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          |
## | GNU General Public License for more details.                          |
## +-----------------------------------------------------------------------+
##Credit to this fine project from the very first version on has to go to Shane Arndrusiak.
##If he had not started this project there never would have been a NukeLadder.
##Though he has gone on to other things let it be known we all appreciate what he started.
###############################################################


if ( !defined('MODULE_FILE') ) {
	die("Illegal Module File Access");
}
@require_once("mainfile.php");
define('X1plugin_include', true);
require_once("modules/extremetournamentsystem/includes/X1File.class.php");

include("header.php");
//title(""._RECOMMEND."");
OpenTable();
define('parent_path', 'modules/extremetournamentsystem/');
require_once(parent_path."nukeladdersystem.php");
CloseTable();
include('footer.php');