<?php
/*############################
File:event.php
Needs:N/A
returns:$event set as X1LadderMod
What does it do: Sets global $event to X1LadderMod
#############################*/
	//this is for loading of the files of your event.
	//The language file
	X1File::X1LoadFile(X1_corelang.".php",X1_modpath."/".basename(dirname(__FILE__))."/language/");
	//The acutal code for the event.
	X1File::X1LoadFile("X1MyEventMod.php", X1_modpath."/".basename(dirname(__FILE__))."/");
	global $gx_event_manager; 
	//Right here you will need to have $gx_event_manager = new youreventMod(); inorder to make sure your event code can be called.
	$gx_event_manager = new X1MyEventMod();
?>
