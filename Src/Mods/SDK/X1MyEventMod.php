<?php
/*#############################################################
##X1plugin Competition Management
##Homepage::http://www.aodhome.com
##Copyright:: Kris Sherrerd 2008-2011
##Version 2.6.4
#############################################################*/
if (!defined('X1plugin_include'))exit();
/*#############################################################
## Event Mod:
## Homepage:
## File: X1MyEventMod.php
## Author:
## Version:
#############################################################*/

/*######################################################
#Class: X1MyEventMod  (Version 0.0.1)   
#implements: X1Eventmods
#What and why: This is the ___ event.  ___ is defined as follows:
Teams and original position:
Winning a match means:
Losing a match means:
######################################################*/
class X1MyEventMod implements X1EventMods{
	
/*######################################
Name:X1AcceptChallenge
Needs:array $maps, array $challenge, array $event, array $team_names
Returns: bool $success
What does it do:Takes the information provided and inserts the event into
the challenge team table making the challenge an official challenge.
#######################################*/	 
/*
Array $maps -> an array of maps choosen for event
Array $challege -> Info from database, all info from table team challenges
Array $event -> Info from database, all info from the ladder information for the event.
Array $team_names -> an array of the actual team names based on team_id.  accessed via :$team_names[$challenge['loser'] or $team_names[$challenge['winner']
*/
    public function X1AcceptChallenge($challenge, $event, $maps, $team_names){
		$suc_count=0;	
		$maps1=explode(",",$challenge['map1']);
		$maps2=implode(",",$maps);
		
		/*This is the best place to do any last checks before the challenge actually happens.*/
		
		/*Sets the challenge info in team events for id winner*/				//Can set what to say when a challenge has been accepted @ HERE
		$success[$suc_count++]=ModifySql("UPDATE", X1_DB_teamsevents, " SET challenged =".HERE." WHERE team_id=".MakeItemString($challenge['winner'])." AND ladder_id=".MakeItemString($event['sid']));
		/*Sets the challenge info in team events for id loser*/
		$success[$suc_count++]=ModifySql("UPDATE", X1_DB_teamsevents, " SET challenged =".HERE." WHERE team_id=".MakeItemString($challenge['loser'])." AND ladder_id=".MakeItemString($event['sid']));
	
		//The official chellenge made.
		$success[$suc_count]=ModifySql("update", X1_DB_teamchallenges, "Set
		ctemp=".MakeItemString(0).",
		date=".MakeItemString(time()).", 
		map1=".MakeItemString($challenge['map1']).",
		map2=".MakeItemString($maps2).",
		matchdate=".MakeItemString(DispFunc::X1Clean($_POST['matchdate']))."
		where randid=".MakeItemString($challenge['randid']));
		
		for($i=0;$i<$suc_count;$i++)
		{
			if(!$success[$i]){
				//debugging
				//echo $i 
				return false;
			}
		}
		return true;
	}

/*######################################
Name:X1SetChallenge
Needs:array maps, array dates, int $randid, databaseinfo $rchallenged, databaseinfo $rchallenger databaseinfo $challenger, databaseinfo $challenged, databaseinfo $event  
Returns: bool $success
What does it do:Takes the inforation provided and inserts the event into
the tempchallenge table, this means a team has been challenged but has yet to accept or decline.
#######################################*/	
/*
Array $maps -> an array of maps choosen for event
Array $dates -> an array of dates for the match
int $randid -> The event ID
Array $challenger -> Info from database, all info from the team table for challenger
Array $challenged -> Info from database, all info from the team table for challenged
Array $event -> Info from database, all info from the ladder information for the event.
Array $rchallenger -> Info from database, all info from the teamevent table for challenger
Array $rchallenged -> Info from database, all info from the teamevent table for challenger
*/     
	public function X1SetChallenge($maps, $dates, $randid, $challenger, $challenged, $event, $rchallenged='', $rchallenger='')		
	{
	 	$suc_count=0;
		//Implode For Storage in Database
		$mapentry=implode(",",$maps);
		$dateentry=implode(",",$dates);
		
		//Best place to do checks to see if your event is ok with the challenge.
		
		//Update the database for the challngers/ed.						//Can set what to say when a challenge has been sent @ HERE
		$success[$suc_count++]=ModifySql("UPDATE", X1_DB_teamsevents, "SET challenged=".HERE." WHERE team_id=".MakeItemString($challenged['team_id'])." AND ladder_id=".MakeItemString($event['sid']));
		
		$success[$suc_count++]=ModifySql("UPDATE", X1_DB_teamsevents, "	SET challenged=".HERE." WHERE team_id=".MakeItemString($challenger['team_id'])." AND ladder_id=".MakeItemString($event['sid']));
		
		//Set a challenge in motion.  No other information is recorded here then the very basic, a challege is in motion.
		$success[$suc_count++]=ModifySql("INSERT INTO", X1_DB_teamchallenges, "
		(ctemp,winner, loser, date, randid, ladder_id, map1, matchdate) 
		VALUES(
		".MakeItemString(1).",
		".MakeItemString($challenged['team_id']).", 
		".MakeItemString($challenger['team_id']).", 
		".MakeItemString(time()).", 
		".MakeItemString($randid).", 
		".MakeItemString($event['sid']).",
		".MakeItemString($mapentry).",
		".MakeItemString($dateentry)."
		)");

		for($i=0;$i<$suc_count;$i++)
		{
			if(!$success[$i]){
			 //debugging
			 //	echo $success[$i];
				return false;
			}
		}
		return true;
	}
	
/*######################################
Name:X1JoinEvent
Needs:int $numteamsonladder, int $team_id, databaseinfo $lad, databaseinfo $teaminfo, array extainto
Returns: bool $success
What does it do:Takes the inforation provided puts the team into said event.
#######################################*/
/*
int $numteamsonladder -> number of teams in the event.
int $team_id ->  Id of team entering event
int $lad -> Info from database, all info from the ladder information for the event.
array $teaminfo -> Info from database, all info from the team table
array $extainto -> Reserved for future use.
*/	
	public function X1JoinEvent($numteamsonladder, $team_id, $lad, $teaminfo, $extainto=0){
		
		//This is where the event is defined for the team.  It needs at a minimal the ladder id and team id.  Beyond that it's up to you.
		$result=ModifySql("INSERT INTO ",X1_DB_teamsevents," (ladder_id, team_id)
		VALUES(
		".MakeItemString($lad['sid']).", 
		".MakeItemString($team_id).")");	
	
		
		if($result){
			return true;
		}
		//debug
		//echo "fail";
		return false;
	}
	
/*######################################
Name:X1DeclineChallenge
Needs:int $newpoints, databaseinfo $challenge, databaseinfo $event
Returns: bool $success
What does it do:Takes the information provided, and removes the challenge from the tempchallenge database. 
#######################################*/
/*
int $newpoints -> Point value
int $newtotalpoints -> New Point value
array $challenge -> Info from database, all info from the teamchallenge table.
array $event -> Info from database, all info from the ladder information for the event.
*/
	
	public function X1DeclineChallenge($newpoints, $newtotalpoints, $challenge, $event){
		$suc_count=0;
		
		//Update the declining team																								//define here how penalties will work for declining an event.
		$success[$suc_count++]=ModifySql("UPDATE", X1_DB_teamsevents, "SET challyesno ='No', challenged ='".laddermod_openchall."', points = '$newpoints'  HERE    WHERE team_id=".MakeItemString($challenge['winner'])." AND ladder_id=".MakeItemString($event['sid']));
		
		//update the other team
		$success[$suc_count++]=ModifySql("UPDATE", X1_DB_teamsevents, "SET challyesno ='No', challenged ='".laddermod_openchall."' WHERE team_id=".MakeItemString($challenge['loser'])." AND ladder_id=".MakeItemString($event['sid']));
		
		//remove the teams from the challenge.
		$success[$suc_count++]=ModifySql("DELETE FROM", X1_DB_teamchallenges, " WHERE randid = ".MakeItemString($challenge['randid']));
		
		//update the declining teams main team info for points
		$success[$suc_count++]=ModifySql("UPDATE", "teams", " SET totalpoints = ".MakeItemString($newtotalpoints)." WHERE team_id=".MakeItemString($challenge['winner']));
		
		for($i=0;$i<$suc_count;$i++)
		{
			if(!$success[$i]){
				//debug
				//echo $i;
				return false;
			}
		}
		return true;
	}
	
/*######################################
Name:X1ModInfo
Needs:N/A
Returns: string $outpout
What does it do:Displays the info about the mod.
#######################################*/			
	public function X1ModInfo(){
		$output = DispFunc::X1PluginTitle(event title);
		$output .= "
			<table class='".X1plugin_mapslist."' width='100%'>
		    	<thead class='".X1plugin_tablehead."'>
					<tr>
						<th>&nbsp;</th> "/*<- Reserved for other information or pictures as apropraite*/"
					</tr>
				</thead>
				<tbody class='".X1plugin_tablebody."'>
					<tr>
						<td class='alt1'>".Your event information."</td>
					</tr>
				</tbody>
				<tfoot class='".X1plugin_tablefoot."'>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</tfoot>
			</table>";
			return $output;
	}
	
/*######################################
Name:X1QuitEvent
Needs:array $lad, int team_id
Returns: bool $success
What does it do:Removed the team from the event
#######################################*/
/*
array $lad -> Info from database, all info from the ladder information for the event.
int $team_id -> The id of the team to remove.
*/
	public function X1QuitEvent($lad, $team_id){
		//use this space to do any fiddling with the information before being rid of the team.
		
		//removed the team from the event.
		$del= ModifySql("DELETE FROM ",X1_DB_teamsevents," WHERE ladder_id=".MakeItemString($lad['sid'])." AND team_id=".MakeItemString($team_id));
		
		if($del){
			//debug
			//echo "Fail";
			return false;
		}
		return true;
	}
	
/*######################################
Name:X1ReportDraw
Needs:array $ids(string $winner, string $loser_id, string $loser, string $winner_id), array $mapnumarray, array $m1winnerarray, array $m1loserarray, array $m2winnerarray, array $m2loserarray, databaseinfo $challenge,databaseinfo $event
Returns: bool $success
What does it do:Reports a draw for said event
#######################################*/		
/*
array $ids -> An array of the winner and loser team names and ids
array $mapnumarray -> Array of the maps
array $m1winnerarray -> Array of the winner score by map
array $m1loserarray -> Array of the loser score by map
array $m2winnerarray -> Array of the winner score by map
array $m2loserarray -> Array of the loser score by map
array $challenge -> Info from database, all info from the ladder information for the event.
array $event -> Info from database, all info from the ladder information for the event.
*/
	public function X1ReportDraw($ids,$mapnumarray,$m1winnerarray,$m1loserarray,$m2winnerarray,$m2loserarray, $challenge, $event){
	 
		$winner=$ids['winner'];
	 	$winner_id=$ids['winner_id'];
		$loser=$ids['loser'];
	 	$loser_id=$ids['loser_id'];
	 	$suc_count=0;
		
		//In here you may want to modify/update information at appropraite for a draw.
		
		//Update losing teams record in the datebase for the event
		$success[$suc_count++]=ModifySql("UPDATE", X1_DB_teamsevents, 
		"SET draws=draws+1, 
		challenged ='".laddermod_drawvs."$winner', 
		points=points+$event[pointsdraw], 
		games=games+1, 
		streakwins=0, 
		streaklosses=0 
		WHERE team_id=".MakeItemString($loser_id)."	AND ladder_id=".MakeItemString($event['sid']));
		
		//Update winner teams record in the datebase for the event
		$success[$suc_count++]=ModifySql("UPDATE", X1_DB_teamsevents, 
		"SET draws=draws+1, 
		challenged ='".laddermod_drawvs."$loser', 
		points=points+$event[pointsdraw], 
		games=games+1, 
		streakwins=0, 
		streaklosses=0 
		WHERE team_id=".MakeItemString($winner_id)." AND ladder_id=".MakeItemString($event['sid']));
			
		//Update overall teams record for the loser.
		$success[$suc_count++]=ModifySql("UPDATE", X1_DB_teams, 
		"SET totaldraws=totaldraws+1, 
		totalpoints=totalpoints+$event[pointsdraw],  
		totalgames=totalgames+1 
		WHERE team_id=".MakeItemString($loser_id));
		
		//Update overall teams record for the winner.
		$success[$suc_count++]=ModifySql("UPDATE", X1_DB_teams, 
		"SET totaldraws=totaldraws+1, 
		totalpoints=totalpoints+$event[pointsdraw],  
		totalgames=totalgames+1 
		WHERE team_id=".MakeItemString($winner_id));
		
		for($i=0;$i<sizeof($success);$i++)
		{
			if(!$success[$i]){
				//debug
				//echo $i;
				return false;
			}
		}
		return true;
	}

/*######################################
Name:X1ReportLoss
Needs:array $ids(string $winner, string $loser_id, string $loser, string $winner_id), array $mapnumarray, array $m1winnerarray, array $m1loserarray, array $m2winnerarray, array $m2loserarray, databaseinfo $challenge,databaseinfo $event
Returns: bool $success
What does it do:Reports a loss for said event
#######################################*/
/*
array $ids -> An array of the winner and loser team names and ids
array $mapnumarray -> Array of the maps
array $m1winnerarray -> Array of the winner score by map
array $m1loserarray -> Array of the loser score by map
array $m2winnerarray -> Array of the winner score by map
array $m2loserarray -> Array of the loser score by map
array $challenge -> Info from database, all info from the ladder information for the event.
array $event -> Info from database, all info from the ladder information for the event.
*/
	public function X1ReportLoss($ids,$mapnumarray,$m1winnerarray,$m1loserarray,$m2winnerarray,$m2loserarray, $challenge, $event)
	{
	 	$suc_count=0;
	 	
		$winner=$ids['winner'];
	 	$winner_id=$ids['winner_id'];
	 	$loser=$ids['loser'];
	 	$loser_id=$ids['loser_id'];
	 	
		//In here you may want to modify/update information at appropraite for a draw.
		
			
		//Update losing teams event record 
		$success[$suc_count++] = ModifySql("UPDATE", X1_DB_teamsevents, "SET 
		challenged ='".laddermod_defeateddby."$winner', 
		wins=wins, 
		losses=losses+1, 
		points=points+$event[pointsloss], 
		games=games+1,
		streakwins=0, 
		streaklosses=streaklosses+1 
		WHERE team_id=".MakeItemString($loser_id)." AND ladder_id=".MakeItemString($event['sid']));
		
		
		//Update winning teams event record.
		$success[$suc_count++] = ModifySql("UPDATE", X1_DB_teamsevents, "SET 
		challenged ='".laddermod_defeated." $loser', 
		wins=wins+1, 
		losses=losses, 
		points=points+$event[pointswin], 
		games=games+1, 
		streakwins=streakwins+1, 
		streaklosses=0 
		WHERE team_id=".MakeItemString($winner_id)." AND ladder_id=".MakeItemString($event['sid']));
		
		//Update losing teams main record.
		$success[$suc_count++] = ModifySql("UPDATE", X1_DB_teams, "SET 
		totalwins=totalwins, 
		totallosses=totallosses+1, 
		totalpoints=totalpoints+$event[pointsloss], 
		totalgames=totalgames+1
		WHERE team_id=".MakeItemString($loser_id));
		
		//Update winning teams main record.
		$success[$suc_count++] = ModifySql("UPDATE", X1_DB_teams, "SET 
		totalwins=totalwins+1, 
		totallosses=totallosses, 
		totalpoints=totalpoints+$event[pointswin], 
		totalgames=totalgames+1 
		WHERE team_id=".MakeItemString($winner_id));
		
		for($i=0;$i<$suc_count;$i++)
		{
			if(!$success[$i]){
				//debug
				//echo $i;
				return false;
			}
		}
		return true;		
	
	}
	
/*######################################
Name:X1ResetEvent
Needs:int $ladder_id
Returns: bool $success
What does it do:resets stats for all teams on said event, does NOT remove points from a teams total points (located out side said event). 
#######################################*/
/*
int $ladder_id -> Events id;
*/
		
	public function X1ResetEvent($ladder_id){
		$not_succ=false;

		//You may have more to do then to zero out all values when you actually reset your event.  Do it here
		
		$not_succ = ModifySql("UPDATE", X1_DB_teamsevents, "
		SET challenged ='".laddermod_openchall."',
		wins=0, 
		losses=0, 
		draws=0, 
		points=0, 
		games=0, 
		streakwins=0, 
		streaklosses=0, 
		penalties=0, 
		rung=0 
		WHERE ladder_id=".MakeItemString($ladder_id));
		

		if($not_succ){
			//debug
			//echo "fail";
			return false;
		}
		return true;  //Database updated successfully.
	}

/*######################################
Name:X1Standings
Needs:int $sid=0, string $limit="", int $start=0
Returns: string
What does it do:creates the standings for players on the ladder
#######################################*/		
/*
array $event -> Info from database, all info from the ladder information for the event.
array $game  -> Info from database, all info from the game information.
int $sid -> event id;
int $limit -> limit number of teams sent.
int $start -> start of the teams gotten from the database.
int $numberofplayersin -> number of teams in event.
*/
	public function X1Standings($event, $game, $sid, $limit="", $start=0, $numberofplayersin=0){
	//Please note this is just a default, the two areas that have html can be changed as much as you want.
		$span = 11;// span
		//event title
		$output = DispFunc::X1PluginTitle(laddermod_leaderboard.$event['title']);
		//here is really where html knowledge comes into play.  You can redo this as appropriate.
		$output .= "
		<table class='".X1plugin_standingstable."' width='100%'>
	    <thead class='".X1plugin_tablehead."'>
	    	<tr>
	    		<th class='alt3'>".laddermod_rank."</th>
	    		<th class='alt3'>".laddermod_tags." </th>
	    		<th class='alt3'>".laddermod_team."</th>
	    		<th class='alt3'>".laddermod_status."</th>
	    		<th class='alt3'>".laddermod_wins."</th>
	    		<th class='alt3'>".laddermod_losses."</th>
	    		<th class='alt3'>".laddermod_draws."</th>
	    		<th class='alt3'>".laddermod_points."</th>
	    		<th class='alt3'>".laddermod_percentage."</th>
				<th class='alt3'>".laddermod_streak."</th>
	    		<th class='alt3'>".laddermod_country."</th>
	    	</tr>
	    </thead>
	    <tbody class='".X1plugin_tablebody."'>";
		//If your using pages leave this.
    if(isset($_REQUEST['page'])){
    	$cur_page=DispFunc::X1Clean($_REQUEST['page']);
    }
    else{
    	$cur_page=1;
    }
    //limiting the number of teams that are retrieved from the database at a time.
    if(isset($_REQUEST['limit'])){
			$limit=DispFunc::X1Clean($_REQUEST['limit']);
		}
		else{
		 	$limit=X1_topteamlimit;
		}
		//The current page after calculations.
		$start = $cur_page * $limit - $limit;
		if(empty($start)){
			$start = 0;
		}
		//getting the action operator.
		$op=DispFunc::X1Clean($_REQUEST[X1_actionoperator]);
		$limit = ($op == "standings") ? "" :" LIMIT $start, $limit";
		
		//the call to the database to get the teams
		$team_info = SqlGetAll(X1_prefix.X1_DB_teamsevents.".*, ".X1_prefix.X1_DB_teams.".country, ".X1_prefix.X1_DB_teams.".name, ".X1_prefix.X1_DB_teams.".clantags",X1_DB_teamsevents.",".X1_prefix.X1_DB_teams," WHERE ".X1_prefix.X1_DB_teamsevents.".ladder_id=".MakeItemString($sid)." and ".X1_prefix.X1_DB_teams.".team_id=".X1_prefix.X1_DB_teamsevents.".team_id"," ORDER BY rung asc ".$limit);
		if($team_info){
			//rank only important if your wanting to have teams numbered.
			$rank = 1+$start;
			//Where each teams information is put out to the world.  HTML is a must to get this as you want it.
			foreach($team_info AS $team){
				//next 2 lines are specific to an event.  They simply are: percentage A % of win, Played total games played.
				$played = $team["wins"]+$team["losses"]+$team['draws'];
				$percentage = ($played <= 0)?0.00: round($team["wins"]/$played, 2)*100;
				//A switch case that determins hte winning streak.
				switch($team["streakwins"]){
					case 4:
						$streak = "<img src='".X1_imgpath."/stars/stars-4.gif' title='$team[streakwins] ".laddermod_winsinarow."'>";
						break;
					case 3:
						$streak = "<img src='".X1_imgpath."/stars/stars-3.gif' title='$team[streakwins] ".laddermod_winsinarow."'>";
						break;
					case 2:
						$streak = "<img src='".X1_imgpath."/stars/stars-2.gif' title='$team[streakwins] ".laddermod_winsinarow."'>";
						break;
					case 1:
						$streak = "<img src='".X1_imgpath."/stars/stars-1.gif' title='$team[streakwins] ".laddermod_winsinarow."'>";
						break;
					case 0:
						$streak = "<img src='".X1_imgpath."/stars/stars-0.gif' title='$team[streakwins] ".laddermod_winsinarow."'>";
						break;
					default:
						$streak = "<img src='".X1_imgpath."/stars/stars-5.gif' title='$team[streakwins] ".laddermod_winsinarow."'>";
						break;
				}
				//This is where the infomation is written out.
				$name2 = str_replace(' ', "+", $team["name"]);
				$output .=  "
				<tr>
					<td class='alt1'>$rank($team[rung])</td>
					<td class='alt2'>$team[clantags]</td>
					<td class='alt1'>
					<a href='".X1_publicgetfile."?".X1_linkactionoperator."=teamprofile&teamname=$name2'>$team[name]</a></td>
					<td class='alt2'>$team[challenged]</td>
					<td class='alt1'>$team[wins]</td>
					<td class='alt2'>$team[losses]</td>
					<td class='alt1'>$team[draws]</td>
					<td class='alt2'>$team[points]</td>
					<td class='alt1'>$percentage%</td>
					<td class='alt2'>$streak</td>
					<td class='alt1'><img src='".X1_imgpath."/flags/$team[country].bmp' align='absmiddle'></td>
				</tr>";
				$rank++;
			}
		}
		//If there were no teams it says this
		else{
			$output .="<tr>
						<td colspan='$span' class='alt1'>".laddermod_noteams."</td>
					</tr>";
		}
		//The next 2 lines determine the page.
		$pages = DispFunc::X1Pagination($numberofplayersin, X1_topteamlimit, 'limit', $cur_page, 'page', X1_publicgetfile."?".X1_linkactionoperator."=ladderhome&sid=$sid");
		$pages = ($op != "standings") ? $pages : "&nbsp;";
		//output of the page footer.
		$output .=  DispFunc::DisplaySpecialFooter($span,true,$pages);
			
		return $output;
	}
	
/*######################################
Name:X1WithdrawChallenge
Needs:databaseinfo $challenge, databaseinfo $event
Returns: bool $success
What does it do:Withdrawls a challenge that was out forth  (The assuming the other team has NOT yet accepted.)
#######################################*/	
/*
array $challenge -> Info from database, all info from the teamchallenge information.
array $event -> Info from database, all info from the ladder information for the event.
*/
	public function X1WithdrawChallenge($challenge, $event){
	$suc_count=0;
	//update the withdrawling team to have challenge set to open  Other consequences can be determined her as well.
	$success[$suc_count++] = ModifySql("UPDATE", X1_DB_teamsevents, "SET challenged ='".laddermod_openchall."' WHERE name=".MakeItemString($challenge['winner'])." AND ladder_id=".MakeItemString($event['sid']));
	//update the other team to have challenge set to open Other consequences can be determined her as well.
	$success[$suc_count++] = ModifySql("UPDATE", X1_DB_teamsevents, "SET challenged ='".laddermod_openchall."' WHERE name=".MakeItemString($challenge['loser'])." AND ladder_id=".MakeItemString($event['sid']));
	//removes the challenge.
	$success[$suc_count++] = ModifySql("DELETE FROM", X1_DB_teamchallenges, "WHERE randid=".MakeItemString($challenge['randid']));
	
		for($i=0;$i<$suc_count;$i++)
		{
			if(!$success[$i]){
				//debug
				//echo $i;
				return false;
			}
		}
		return true;
	}
	
/*######################################
Name:X1DisplaySpecialFeatures
Needs:boolean $edit=false, array $event=0
Returns: String $output
What does it do:If there are specail requirements for an event will display them in the event creation page
#######################################*/		
/*
boolean $edit -> Determines if this is part of creating an event or editing an event.  False=creating an event.
array $event -> If your editing an event this will hold the information you want to display
*/
	public function X1DisplaySpecialFeatures($edit=false,$event=0){
		if(!$edit){
			//creating an event here
			//XL_aevents_lex1,XL_aevents_lex2,... is what your event is using other then points.
			//You can have other things here other then text input
			return $output ="<tr>
    			<td class='alt1'>".XL_aevents_lex1."</td>
    			<td class='alt1'><input type='text' name='"/*What you want to name your info*/"' size='20' value='1'> </td>
    		</tr>
			<tr>
    			<td class='alt1'>".XL_aevents_lex2."</td>
    			<td class='alt1'><input type='text' name='"/*What you want to name your info*/"' size='20' value='1'> </td>
    		</tr>
			...";
		}
		else{
			//Editing an event here
			//XL_aevents_lex1,XL_aevents_lex2,... is what your event is using other then points.
			//You can have other things here other then text input
			return $output ="<tr>
    			<td class='alt1'>".XL_aevents_lex1."</td>
    			<td class='alt1'><input type='text' name='"/*What you called your info*/"' size='20' value='".$event['your database column']."'> </td>
    		</tr>
			<tr>
    			<td class='alt1'>".XL_aevents_lex2."</td>
    			<td class='alt1'><input type='text' name='"/*What you want to name your info*/"' size='20' value='".$event['your database column']."'> </td>
    		</tr>
			...";
		}	
	}
	
/*######################################
Name:X1HasSpecialFeatures
Needs:boolean $edit=false, databaseinfo $event
Returns: bool $hasspecial
What does it do:If there are special features to be seen it returns true other wise it returns false.
#######################################*/			
	public function X1HasSpecialFeatures(){
		//If you aren't going to be using X1DisplaySpecialFeatures then please just return false here.
		return true;
	}
	
/*######################################
Name:X1DataInsert
Needs:boolean $edit=false
Returns: array of 2 strings.
What does it do:Takes the information needed for a special event, both the needed col names of the
database and the $_POST of said special features, and puts it in the form required to run the db 
function.
#######################################*/
/*
boolean $edit -> Determines if this is part of creating an event or editing an event.  False=creating an event.
*/
	public function X1DataInsert($edit=false){
		if(!$edit){
			//creating an event here
			$db_string1="Your database(s) column,...";
			$db_string2=MakeItemString(Dispfunc::X1Clean($_POST['Your database column'])).", "..", ...";
			return array($db_string1,$db_string2);
		}
		else{
			//editing an event here
			$db_string1="Your database column=".MakeItemString(Dispfunc::X1Clean($_POST['Your database column']));
			return array($db_string1);
		}
	}
	
	//Below here you can create private functions as you see fit.
}
?>
