<?php
###############################################################
##Nuke Ladder - XTS
##Homepage::http://www.aodhome.com
##Copyright:: Shane Andrusiak 2000-2006   Kris Sherrerd 2008-2010 (2.6.0)
##Version 2.6.4
###############################################################
#Plugin Location
$path = "../modules/extremetournamentsystem";
if(file_exists($path.'/xts_config.php')){
		require_once($path.'/xts_config.php');
}
else{
	print die_table("Cant Load Configuration", "Cant find xts_config.php, please make sure you have uploaded all files. (If you have changed the name of the directory you need to edit the file in Install/installers/uninstall.php  Change the path at the top of the page.)");
	$exit = true;
}
if(file_exists($path.'/includes/adodb/adodb.inc.php')){
	require_once($path.'/includes/adodb/adodb.inc.php');
	$xdb = ADONewConnection('mysql');
	$result = $xdb->Connect($_POST['dbhost'],$_POST['dbuname'],$_POST['dbpass'],$_POST['dbname'] );
	$ADODB_FETCH_MODE = 'ADODB_FETCH_ASSOC';
}
else{
	print die_table("Cant Load Adodb", "Cant find adodb, please make sure you have entered the correct path location to the Nukeladder files");
	$exit = true;
}
if(!$exit){
	if(!$result){
		print die_table("Could not connect to the database.", "Please check your databse connection details and try again"); 
	}
	else{
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_teamchallenges);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_teaminvites);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_games);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_teamdisputes);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_maps);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_events);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_teamsevents);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_mapgroups);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_messages);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_nukstaff);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_teamhistory);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_teams);
		$xdb->Execute("DROP TABLE ".X1_prefix.X1_DB_teamroster);

		print die_table("Uninstallation Complete", "Uninstallation has finished, please remove the install folder from your server");
	}
}
?>