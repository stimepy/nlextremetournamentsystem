<?php
if (eregi("block-NukeLadder2Stats.php", $PHP_SELF)) {
    Header("Location: index.php");
    die();
}

global $prefix, $db;
$sql="SELECT * FROM ".$prefix."_games";
$result= $db->sql_query($sql);
$numgames = $db->sql_numrows($result);
$sql="SELECT * FROM ".$prefix."_ladders";
$result= $db->sql_query($sql);
$numladder = $db->sql_numrows($result);
$sql="SELECT * FROM ".$prefix."_laddermaplist";
$result= $db->sql_query($sql);
$nummaps = $db->sql_numrows($result);
$sql="SELECT * FROM ".$prefix."_teams";
$result= $db->sql_query($sql);
$numteams = $db->sql_numrows($result);
$sql="SELECT * FROM ".$prefix."_userteams";
$result= $db->sql_query($sql);
$numplayers = $db->sql_numrows($result);
$sql="SELECT * FROM ".$prefix."_playedgames";
$result= $db->sql_query($sql);
$nummatches = $db->sql_numrows($result);
	
$content = "
<strong><big>&middot;</big></strong>&nbsp;($numgames) Total Games<br />
<strong><big>&middot;</big></strong>&nbsp;($numladder) Total Ladders<br />
<strong><big>&middot;</big></strong>&nbsp;($numteams) Total Teams<br />
<strong><big>&middot;</big></strong>&nbsp;($numplayers) Total Players<br />
<strong><big>&middot;</big></strong>&nbsp;($nummatches) Total Matches<br />
<strong><big>&middot;</big></strong>&nbsp;($nummaps) Total Maps
";
?>
