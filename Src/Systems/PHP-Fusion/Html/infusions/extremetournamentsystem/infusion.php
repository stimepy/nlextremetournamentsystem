<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright � 2002 - 2008 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: infusion.php
| Author: INSERT NAME HERE
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied"); }

//include INFUSIONS."infusion_folder/infusion_db.php";

// Check if locale file is available matching the current site locale setting.
if (file_exists(INFUSIONS."extremetournamentsystem/locale/".$settings['locale'].".php")) {
	// Load the locale file matching the current site locale setting.
	include INFUSIONS."extremetournamentsystem/locale/".$settings['locale'].".php";
} else {
	// Load the infusion's default locale file.
	include INFUSIONS."extremetournamentsystem/locale/English.php";
}

// Infusion general information
$inf_title = $locale['xts_title'];
$inf_description = $locale['xts_desc'];
$inf_version = "2.6.4 Beta";
$inf_developer = "Kris Sherrerd(Angelofdoom)";
$inf_email = "stimepy@aodhome.com";
$inf_weburl = "http://www.aodhome.com";

$inf_folder = "extremetournamentsystem"; // The folder in which the infusion resides.

// Delete any items not required below.
$inf_newtable = array( 1=>DB_PREFIX."xts_challengeteam (
  `ctemp` int(1) NOT NULL DEFAULT '1',
  `winner` varchar(255) NOT NULL DEFAULT '',
  `loser` varchar(255) NOT NULL DEFAULT '',
  `date` varchar(255) NOT NULL DEFAULT '',
  `randid` varchar(10) NOT NULL DEFAULT '0',
  `ladder_id` varchar(10) NOT NULL DEFAULT '',
  `map1` varchar(255) NOT NULL DEFAULT 'None',
  `map2` varchar(255) NOT NULL DEFAULT 'None',
  `matchdate` varchar(255) NOT NULL DEFAULT 'None',
  PRIMARY KEY (`winner`,`loser`,`randid`),
  KEY `randid` (`randid`)
) ENGINE=MyISAM;", 
2=>DB_PREFIX."xts_confirminvites (
  `invite_id` int(10) NOT NULL AUTO_INCREMENT,
  `team_id` varchar(40) NOT NULL DEFAULT '',
  `randid` varchar(10) NOT NULL DEFAULT '0',
  `uid` varchar(40) NOT NULL DEFAULT '0',
  PRIMARY KEY (`invite_id`)
) ENGINE=MyISAM;",
3=>DB_PREFIX."xts_games (
  `gameid` int(10) NOT NULL AUTO_INCREMENT,
  `gamename` varchar(32) DEFAULT NULL,
  `gameimage` varchar(20) DEFAULT NULL,
  `gametext` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`gameid`)
) ENGINE=MyISAM;",
4=>DB_PREFIX."xts_ladderdisputes (
  `dispute_id` int(10) NOT NULL AUTO_INCREMENT,
  `sender` varchar(40) NOT NULL DEFAULT '',
  `offender` varchar(40) NOT NULL DEFAULT '',
  `ladder_id` int(5) NOT NULL DEFAULT '0',
  `date` varchar(40) NOT NULL DEFAULT '',
  `info` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`dispute_id`)
) ENGINE=MyISAM;",
5=>DB_PREFIX."xts_laddermaplist (
  `mapid` int(10) NOT NULL AUTO_INCREMENT,
  `mapname` varchar(40) NOT NULL DEFAULT 'default map',
  `mappic` varchar(40) NOT NULL DEFAULT 'none',
  `mapdl` varchar(255) NOT NULL DEFAULT 'none',
  `gpmp_cnt` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mapid`)
) ENGINE=MyISAM;",
6=>DB_PREFIX."xts_ladders (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) DEFAULT NULL,
  `hometext` text,
  `bodytext` text NOT NULL,
  `game` int(3) NOT NULL DEFAULT '1',
  `notes` text NOT NULL,
  `allow_rest` int(1) NOT NULL DEFAULT '0',
  `score` int(10) NOT NULL DEFAULT '0',
  `ratings` int(10) NOT NULL DEFAULT '0',
  `pointswin` int(5) NOT NULL DEFAULT '2',
  `pointsloss` int(5) NOT NULL DEFAULT '0',
  `pointsdraw` int(5) NOT NULL DEFAULT '1',
  `gamesmaxday` int(5) NOT NULL DEFAULT '1',
  `declinepoints` int(5) NOT NULL DEFAULT '1',
  `active` int(11) NOT NULL DEFAULT '1',
  `enabled` int(11) NOT NULL DEFAULT '1',
  `challengelimit` int(5) NOT NULL DEFAULT '1',
  `challengedays` int(40) NOT NULL DEFAULT '7',
  `restrictdates` int(1) NOT NULL DEFAULT '0',
  `numdates` int(5) NOT NULL DEFAULT '3',
  `restrictmaps` int(5) NOT NULL DEFAULT '0',
  `nummaps1` int(5) NOT NULL DEFAULT '3',
  `nummaps2` int(5) NOT NULL DEFAULT '2',
  `standingstype` varchar(255) NOT NULL DEFAULT '',
  `maxteams` int(10) NOT NULL DEFAULT '0',
  `minplayers` int(10) NOT NULL DEFAULT '0',
  `maxplayers` int(10) NOT NULL DEFAULT '500',
  `type` varchar(255) NOT NULL DEFAULT 'league',
  `expirechalls` tinyint(1) NOT NULL DEFAULT '0',
  `expirehours` int(10) NOT NULL DEFAULT '120',
  `expirepen` int(10) NOT NULL DEFAULT '1',
  `expirebon` int(10) NOT NULL DEFAULT '1',
  `whoreports` varchar(10) NOT NULL DEFAULT 'loser',
  `mapgroups` text NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM;",
7=>DB_PREFIX."xts_ladderteams (
  `ladder_id` int(10) NOT NULL DEFAULT '0',
  `team_id` int(10) NOT NULL DEFAULT '0',
  `games` int(10) NOT NULL DEFAULT '0',
  `wins` int(10) NOT NULL DEFAULT '0',
  `losses` int(10) NOT NULL DEFAULT '0',
  `draws` int(10) NOT NULL DEFAULT '0',
  `points` int(100) NOT NULL DEFAULT '0',
  `penalties` int(10) NOT NULL DEFAULT '0',
  `streakwins` int(10) NOT NULL DEFAULT '0',
  `streaklosses` int(10) NOT NULL DEFAULT '0',
  `rest` int(10) NOT NULL DEFAULT '0',
  `challenged` varchar(255) NOT NULL DEFAULT 'New Team',
  `challyesno` char(3) NOT NULL DEFAULT 'No',
  `rung` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ladder_id`,`team_id`)
) ENGINE=MyISAM;",
8=>DB_PREFIX."xts_mapgroups (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `maps` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM;",
9=>DB_PREFIX."xts_messages (
  `randid` int(10) NOT NULL,
  `messid` int(10) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `hasread` int(1) NOT NULL DEFAULT '0',
  `steam_id` int(10) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `rteam_id` int(10) NOT NULL,
  `tstamp` varchar(255) NOT NULL,
  PRIMARY KEY (`randid`,`messid`)
) ENGINE=MyISAM;",
10=>DB_PREFIX."xts_nukladstaff (
  `mod_id` int(10) NOT NULL AUTO_INCREMENT,
  `mod_name` varchar(40) NOT NULL DEFAULT '',
  `mod_pswd` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`mod_id`)
)ENGINE=MyISAM;",
11=>DB_PREFIX."xts_playedgames (
  `game_id` int(10) NOT NULL AUTO_INCREMENT,
  `winner_id` int(255) NOT NULL,
  `winner` varchar(40) DEFAULT NULL,
  `loser_id` int(255) NOT NULL,
  `loser` varchar(40) DEFAULT NULL,
  `date` varchar(40) DEFAULT NULL,
  `map1` varchar(255) NOT NULL DEFAULT 'n/a',
  `map2` varchar(255) NOT NULL DEFAULT 'n/a',
  `mapsettotal` varchar(255) NOT NULL DEFAULT 'n/a',
  `map1t1` varchar(255) NOT NULL DEFAULT 'n/a',
  `map1t2` varchar(255) NOT NULL DEFAULT 'n/a',
  `map2t1` varchar(255) NOT NULL DEFAULT 'n/a',
  `map2t2` varchar(255) NOT NULL DEFAULT 'n/a',
  `scrnsht1` varchar(255) NOT NULL DEFAULT 'n/a',
  `scrnsht2` varchar(255) NOT NULL DEFAULT 'n/a',
  `comments` varchar(255) NOT NULL DEFAULT '',
  `laddername` int(11) NOT NULL DEFAULT '0',
  `draw` tinyint(1) NOT NULL DEFAULT '0',
  `demo` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`game_id`)
) ENGINE=MyISAM;",
12=>DB_PREFIX."xts_teams (
  `team_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '',
  `totalnmessag` int(10) NOT NULL DEFAULT '0',
  `mail` varchar(50) DEFAULT NULL,
  `aim` varchar(40) DEFAULT NULL,
  `icq` varchar(15) DEFAULT NULL,
  `msn` varchar(40) DEFAULT NULL,
  `xfire` varchar(40) DEFAULT NULL,
  `yim` varchar(40) DEFAULT NULL,
  `country` varchar(40) DEFAULT '',
  `totalwins` int(10) DEFAULT '0',
  `totallosses` int(10) DEFAULT '0',
  `totaldraws` int(10) DEFAULT '0',
  `totalpoints` int(10) DEFAULT '0',
  `totalgames` int(10) DEFAULT '0',
  `penalties` int(10) DEFAULT '0',
  `playerone` int(10) NOT NULL DEFAULT '0',
  `playerone2` varchar(255) NOT NULL DEFAULT '',
  `clantags` varchar(10) NOT NULL DEFAULT '',
  `challenged` varchar(10) NOT NULL DEFAULT 'No',
  `website` varchar(200) NOT NULL,
  `clanlogo` varchar(200) NOT NULL DEFAULT '',
  `ircserver` varchar(40) NOT NULL DEFAULT '''',
  `ircchannel` varchar(40) NOT NULL DEFAULT '',
  `joinpassword` varchar(40) NOT NULL DEFAULT '',
  `recruiting` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`team_id`)
) ENGINE=MyISAM;",
13=>DB_PREFIX."xts_userinfo (
  `uid` int(10) NOT NULL,
  `gam_name` varchar(255) NOT NULL DEFAULT 'N/A',
  `p_country` varchar(255) NOT NULL,
  `p_mail` varchar(255) NOT NULL,
  `faux_email` varchar(255) NOT NULL,
  `use_faux` int(10) NOT NULL DEFAULT '0',
  `p_aim` varchar(40) NOT NULL,
  `p_icq` varchar(40) NOT NULL,
  `p_msn` varchar(255) NOT NULL,
  `p_xfire` varchar(40) NOT NULL,
  `p_yim` varchar(255) NOT NULL,
  `p_website` varchar(256) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM;",
14=>DB_PREFIX."xts_userteams (
  `uid` int(10) NOT NULL DEFAULT '0',
  `team_id` int(10) NOT NULL DEFAULT '0',
  `extra1` varchar(255) NOT NULL DEFAULT 'none',
  `extra2` varchar(255) NOT NULL DEFAULT 'none',
  `extra3` varchar(255) NOT NULL DEFAULT 'none',
  `joindate` varchar(10) NOT NULL DEFAULT 'n/a',
  `cocaptain` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`,`team_id`)
) ENGINE=MyISAM;");


$inf_droptable = array(1=>DB_PREFIX."xts_challengeteam",2=>DB_PREFIX."xts_confirminvites",3=>DB_PREFIX."xts_games",
4=>DB_PREFIX."xts_ladderdisputes",5=>DB_PREFIX."xts_laddermaplist",6=>DB_PREFIX."xts_ladders",7=>DB_PREFIX."xts_ladderteams",
8=>DB_PREFIX."xts_mapgroups",9=>DB_PREFIX."xts_messages",10=>DB_PREFIX."xts_nukladstaff",11=>DB_PREFIX."xts_playedgames",
12=>DB_PREFIX."xts_teams",13=>DB_PREFIX."xts_userinfo", 14=>DB_PREFIX."xts_userteams");

/*
$inf_insertdbrow[1] = DB_INFUSION_TABLE." (field1, field2, field3, field4) VALUES('', '', '', '')";
$inf_altertable[1] = DB_INFUSION_TABLE." ADD etc";

$inf_deldbrow[1] = "other_table";
*/
$inf_adminpanel[1] = array(
	"title" => $locale['xts_admin1'],
	"image" => "image.gif",
	"panel" => "xts.php?op=admin&",
	"rights" => "XXX"
);

$inf_sitelink[1] = array(
	"title" => $locale['xts_link1'],
	"url" => "xts.php",
	"visibility" => "0"
);
?>