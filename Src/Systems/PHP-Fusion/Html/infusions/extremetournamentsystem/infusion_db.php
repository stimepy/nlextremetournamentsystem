<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright � 2002 - 2008 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: infusion_db.php
| Author: INSERT NAME HERE
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied"); }

if (!defined("X1_DB_maps")) {
	define('X1_DB_maps', DB_PREFIX.'xts_laddermaplist');
	#Plugin Games Table
	define('X1_DB_games', DB_PREFIX.'xts_games');
	#Plugin Teams Table
	define('X1_DB_teams', DB_PREFIX.'xts_teams');
	#Plugin Events Table
	define('X1_DB_events', DB_PREFIX.'xts_ladders');
	#Plugin Challenges Table
	define('X1_DB_teamchallenges', DB_PREFIX.'xts_challengeteam');
	#Plugin Invites Table
	define('X1_DB_teaminvites', DB_PREFIX.'xts_confirminvites');
	#Plugin Disputes Table
	define('X1_DB_teamdisputes', DB_PREFIX.'xts_ladderdisputes');
	#Plugin Team's Events Table
	define('X1_DB_teamsevents', DB_PREFIX.'xts_ladderteams');
	#Plugin Matches Table
	define('X1_DB_teamhistory', DB_PREFIX.'xts_playedgames');
	#Plugin Joined Teams Table
	define('X1_DB_teamroster', DB_PREFIX.'xts_userteams');
	#Plugin Team User table
	define('X1_DB_userinfo', DB_PREFIX.'xts_userinfo');
	#Plugin Mapgroups Table
	define('X1_DB_mapgroups',DB_PREFIX.'xts_mapgroups');
	#Plugin Staff Table
	define('X1_DB_nukstaff',DB_PREFIX.'xts_nukladstaff');
	#Plugin Message table
	define('X1_DB_messages', DB_PREFIX.'xts_messages');
}
?>