﻿<?php
/*********************************************
  CPG Dragonfly™ CMS
  ********************************************
  Copyright © 2004 - 2005 by CPG-Nuke Dev Team
  http://www.dragonflycms.com

  Dragonfly is released under the terms and conditions
  of the GNU GPL version 2 or any later version

  $Source: /cvs/html/modules/extremetournamentsystem/admin/adlinks.inc,v $
  $Revision: 1.1 $
  $Author: djmaze $
  $Date: 2005/03/24 01:52:37 $
**********************************************/
//Nukeladder- XTS defined on October 3, 2010

$module='extremetournamentsystem';
if (!defined('CPG_NUKE')) { exit; }
	
if (can_admin($module)) {
	$menuitems['_AMENU1'][$module]['URL'] = 'index.php?name='.$module.'&op=admin';
	$menuitems['_AMENU1'][$module]['IMG'] = 'nukeladder';
	$menuitems['_AMENU1'][$module]['MOD'] = 'Nuke Ladder - Extreme Tournament System';
}
