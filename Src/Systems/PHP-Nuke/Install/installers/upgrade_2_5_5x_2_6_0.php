<?php
###############################################################
##Nuke Ladder - XTS
##Homepage::http://www.aodhome.com
##Copyright::  Kris Sherrerd 2008 (2.6.0)
##Version 2.6.0
###############################################################

#Plugin Location
$path = $_POST['path'];
if(!file_exists($path.'/systemconfig.php')){
	print die_table("Cant Load Config", "Cant find config, please make sure you have entered the correct path location to the Nukeladder files");
	$exit = true;
}
if(!file_exists($path.'/adodb/adodb.inc.php')){
	print die_table("Cant Load Adodb", "Cant find adodb, please make sure you have entered the correct path location to the Nukeladder files");
	$exit = true;
}
if(!$exit){
	require_once($path.'/systemconfig.php');
	require_once($path.'/adodb/adodb.inc.php');
	$xdb = ADONewConnection('mysql');
	$result = $xdb->Connect( $_POST['dbhost'] , $_POST['dbuname'], $_POST['dbpass'], $_POST['dbname'] );
	$ADODB_FETCH_MODE =  'ADODB_FETCH_ASSOC';
	if(!$result){
		print die_table("Could not connect to the database.", "Please check your databse connection details and try again"); 
	}
	else{
				#2.6.0
		$xdb->Execute("
			CREATE TABLE ".X1_prefix.X1_DB_nukstaff." (
			mod_id int(10) NOT NULL default '0',
			mod_name varchar(40) NOT NULL default '',
			mod_pswd varchar(40) NOT NULL default '',
			PRIMARY KEY (mod_id)
			) TYPE=MyISAM;
		");
		print die_table("Upgrade Complete", "Upgrade has finished, please remove the install folder from your server");
	}
}
?>