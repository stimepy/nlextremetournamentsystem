<?php
###############################################################
##X1plugin Competition Management
##Homepage::http://www.aodhome.com
##Copyright:: Kris Sherrerd 2008-2011
##Version 2.6.4
###############################################################
// Change, add, modify as you write out what words you are going to use.  These are just some basic ones.
define('yourmod_rank','Rank');
define('yourmod_tags','Tags');
define('yourmod_team','Team');
define('yourmod_status','Status');
define('yourmod_leaderboard', 'Leaderboard  ::  ');
define('yourmod_wins','Wins');
define('yourmod_losses','Losses');
define('yourmod_draws','Draws');
define('yourmod_points','Points');
define('yourmod_percentage','Win %');
define('yourmod_streak','Streak');
define('yourmod_country','Country');
define('yourmod_challengedby', 'Challenged by ');
define('yourmod_challenged', 'Challenged ');
define('yourmod_drawvs', "Draw vs ");
define('yourmod_openchall', 'Open for challenge');
define('yourmod_modinfotitle', 'Ladder Competition');
define('yourmod_modinfodesc', 'Info File Contains Information about the mod, author, settings,  
			this is public and should be treated as such.
			The Ladder competition is based on rung position. 
			Opponnents fight for position and can only challenge above thier position.');
define('yourmod_noteams','No teams have joined this event yet.');
define('XL_aevents_lex1', '');
define('XL_aevents_lex2', '');
?>
