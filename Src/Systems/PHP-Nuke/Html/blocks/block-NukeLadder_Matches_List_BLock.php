<?php
if (preg_match( "/block-NukeLadder2Matches.php/", $PHP_SELF)){//eregi("block-NukeLadder2Matches.php", $PHP_SELF)) {

    Header("Location: index.php");
    die();
}

global $prefix, $db;
$module_name = "extremetournamentsystem";

/*Upcoming Matches section of block*/
/*======================*/
$max="5";
$content = "$prefix<center>::Upcoming Matches::</center><br>";
$content .= "<table width=\"100%\" border=\"0\">";
$start="1";
$result = $db->sql_query("select ta.name as w_name, ct.winner, tb.name as l_name, ct.loser, ct.matchdate, ct.ladder_id from ".$prefix."_xts_challengeteam ct inner join (select team_id, name from ".$prefix."_xts_teams) ta on(ta.team_id=winner) inner join (select team_id, name from ".$prefix."_xts_teams) tb on(tb.team_id=loser) where ctemp<>1");
while(list($w_name, $winner, $l_name, $loser, $matchdate, $ladder_id) = $db->sql_fetchrow($result)) {
	if (($matchdate >= time()) && ($max >= $start)){
		$content .= "
		<tr>
			<td align=\"left\">
				<big>&middot;</big>
				<a href=\"modules.php?name=$module_name&op=teamprofile&teamname=$winner\">$w_name</a> 
				Vs 
				<a href=\"modules.php?name=$module_name&op=teamprofile&teamname=$loser\">$l_name</a>
				<br/>
				<center>
				<a href=\"modules.php?name=$module_name&op=ladderhome&sid=$ladder_id\">(View Ladder)</center>
				<br />
			</td>
		</tr>";
		$start++;
	}
}
$content .= "</table>";


/*Results section of block*/
/*===============*/

$content .= "<center>::Recent Results::</center><br>";
$content .= "<table width=\"100%\" border=\"0\">";
$result = $db->sql_query("
select game_id, winner_id, winner, loser_id, loser, date, game_id, map1t1, map1t2, 
map2t1, map2t2
from ".$prefix."_xts_playedgames order by game_id DESC limit $max");

while(list($game_id, $winner_id, $winner, $loser_id, $loser, $date, $game_id, 
	$map1t1, $map1t2, $map2t1, $map2t2) = $db->sql_fetchrow($result)) {

	$scorewinner = array_sum(explode(",",$map1t1)) + array_sum (explode(",",$map2t1));
	$scoreloser = array_sum(explode(",",$map1t2)) + array_sum (explode(",",$map2t2));
	$date=date("m:d:Y", $date);
    $content .= "
	<tr>
		<td align=\"left\">
			<p>
				<big>&middot;</big>
				<a href=\"modules.php?name=$module_name&op=teamprofile&teamname=$winner_id\">$winner ($scorewinner)</a> 
				Vs 
				<a href=\"modules.php?name=$module_name&op=teamprofile&teamname=$loser_id\">$loser ($scoreloser)</a>
				<br/>
				&middot;$date<br/>
				<a href=\"modules.php?name=$module_name&op=matchdetails&game_id=$game_id\">(Details)</a>
				<br/>
			</p>
		</td>
	</tr>";
}
$content .= "</table>";
?>