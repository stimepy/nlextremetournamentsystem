Welcome to NukeLadder Extreme Tournament System (2.6.4) for PHP-Nuke


BUG REPORTS:
First: http://sourceforge.net/tracker/?func=add&group_id=293105&atid=1238682
Second: http://www.aodhome.com
or
Email: stimepy@aodhome.com




Whats New
-------------
see changelog


Notice:
-------------
Things are still underdevelopment but 99% of this should work.  Any Bugs or problems please report them to: http://sourceforge.net/tracker/?func=add&group_id=293105&atid=1238682


Know Problems:
---------------
None that I can think of, but please report any that you find!



New Installation
-----------------
(Note* If you have run Nukeladder previously and want to start fresh, please uninstall as appropriate and remove any Nukeladder related files.)

-Unzip the the package and locate the 'html' folder.
-Upload the contents of the 'html' folder to your root system directory.
-For PHP-Nuke, Locate the Install folder from the zip file, inside you will find options for php and mysql installs.  
We will use the php install.(skip the next step if you use the .sql file)
-Load the install folder to your root directory
-Run enter the URL  Http://yourwebsite/Install/
-Fill out the appropriate information and click submit.
-Please make sure to delete the Install directory right after you have finished.
-Login to the admin area of your site  and got to modules.
-Enable the Extreme Tournament SYstem.
-TO get the main menu go to the blocks module in the admin area.  Add a new block, use the Nuke Ladder main menu file and create the block.  Make sure to enable it!


Upgrade Installation from Nukeladder 2.5.5x or 2.6.x
-----------------
Nope, not at this time.