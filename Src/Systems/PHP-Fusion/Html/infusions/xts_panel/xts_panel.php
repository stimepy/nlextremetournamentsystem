<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright � 2002 - 2008 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: new_infusion_panel.php
| Author: INSERT NAME HERE
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied"); }


define('XTS_publicgetfile', 'xts.php');
define('XTS_linkactionoperator', 'op');
openside('Tournament');
if(iADMIN){
	$adminlink = "<img src='".THEME."images/bullet.gif' alt=''><a href=\"".BASEDIR."infusions/extremetournamentsystem/xts.php?op=admin\">Admin Panel</a>";
}
else{
	$adminlink=NULL;
}
echo  "
<img src='".THEME."images/bullet.gif' alt=''><a href=\"".BASEDIR."infusions/extremetournamentsystem/".XTS_publicgetfile."?".XTS_linkactionoperator."=home\">Events</a><br />
<img src='".THEME."images/bullet.gif' alt=''><a href=\"".BASEDIR."infusions/extremetournamentsystem/".XTS_publicgetfile."?".XTS_linkactionoperator."=myteams\">Team Administration</a><br />
<img src='".THEME."images/bullet.gif' alt=''><a href=\"".BASEDIR."infusions/extremetournamentsystem/".XTS_publicgetfile."?".XTS_linkactionoperator."=createteam\">CreateTeam</a><br />
<img src='".THEME."images/bullet.gif' alt=''><a href=\"".BASEDIR."infusions/extremetournamentsystem/".XTS_publicgetfile."?".XTS_linkactionoperator."=jointeamform\">Join Team</a><br />
<img src='".THEME."images/bullet.gif' alt=''><a href=\"".BASEDIR."infusions/extremetournamentsystem/".XTS_publicgetfile."?".XTS_linkactionoperator."=quitteamform\">Quit Team</a><br />
<img src='".THEME."images/bullet.gif' alt=''><a href=\"".BASEDIR."infusions/extremetournamentsystem/".XTS_publicgetfile."?".XTS_linkactionoperator."=teamlist\">Team List</a><br />
$adminlink";
closeside();


?>