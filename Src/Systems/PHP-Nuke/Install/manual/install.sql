CREATE TABLE nuke_xts_challengeteam (
  winner varchar(255) NOT NULL default '',
  loser varchar(255) NOT NULL default '',
  date varchar(255) NOT NULL default '',
  randid varchar(10) NOT NULL default '0',
  ladder_id varchar(10) NOT NULL default '',
  map1 varchar(255) NOT NULL default 'None',
  map2 varchar(255) NOT NULL default 'None',
  map3 varchar(255) NOT NULL default 'None',
  matchdate varchar(255) NOT NULL default 'None',
  extra1 varchar(255) NOT NULL default 'n/a',
  extra2 varchar(255) NOT NULL default 'n/a',
  extra3 varchar(255) NOT NULL default 'n/a'
) TYPE=MyISAM;


CREATE TABLE nuke_xts_challengeteamtemp (
  winner varchar(40) NOT NULL default '',
  loser varchar(40) NOT NULL default '',
  date varchar(255) NOT NULL default '',
  randid varchar(10) NOT NULL default '0',
  ladder_id varchar(10) NOT NULL default '',
  map1 varchar(255) NOT NULL default 'None',
  map2 varchar(255) NOT NULL default 'None',
  date1 varchar(255) NOT NULL default 'None',
  date2 varchar(255) NOT NULL default 'None',
  date3 varchar(255) NOT NULL default 'None',
  time1 varchar(255) NOT NULL default 'None',
  time2 varchar(255) NOT NULL default 'None',
  time3 varchar(255) NOT NULL default 'None',
  extra1 varchar(255) NOT NULL default 'n/a',
  extra2 varchar(255) NOT NULL default 'n/a',
  extra3 varchar(255) NOT NULL default 'n/a'
) TYPE=MyISAM;

CREATE TABLE nuke_xts_confirminvites (
  invite_id int(10) NOT NULL auto_increment,
  team_id varchar(40) NOT NULL default '',
  randid varchar(10) NOT NULL default '0',
  uid varchar(40) NOT NULL default '0',
  PRIMARY KEY  (invite_id)
) TYPE=MyISAM;

CREATE TABLE nuke_xts_games (
  gameid int(3) NOT NULL auto_increment,
  gamename varchar(20) default NULL,
  gameimage varchar(20) default NULL,
  gametext varchar(40) default NULL,
  counter int(11) NOT NULL default '0',
  PRIMARY KEY  (gameid)
) TYPE=MyISAM;

CREATE TABLE nuke_xts_ladderdisputes (
  dispute_id tinyint(5) NOT NULL auto_increment,
  sender varchar(40) NOT NULL default '',
  offender varchar(40) NOT NULL default '',
  ladder_id int(5) NOT NULL default '0',
  date varchar(40) NOT NULL default '',
  info varchar(255) NOT NULL default '',
  PRIMARY KEY  (dispute_id)
) TYPE=MyISAM;

CREATE TABLE nuke_xts_laddermaplist (
  mapid int(11) NOT NULL auto_increment,
  mapname varchar(40) NOT NULL default 'default map',
  mappic varchar(40) NOT NULL default 'none',
  mapdl varchar(255) NOT NULL default 'none',
  ladder_id int(10) NOT NULL default '0',
  PRIMARY KEY  (mapid)
) TYPE=MyISAM;

CREATE TABLE nuke_xts_ladders (
  sid int(11) NOT NULL auto_increment,
  catid int(11) NOT NULL default '0',
  aid varchar(30) NOT NULL default '',
  title varchar(80) default NULL,
  time datetime default NULL,
  hometext text,
  bodytext text NOT NULL,
  comments int(11) default '0',
  counter mediumint(8) unsigned default NULL,
  game int(3) NOT NULL default '1',
  informant varchar(20) NOT NULL default '',
  notes text NOT NULL,
  ihome int(1) NOT NULL default '0',
  alanguage varchar(30) NOT NULL default '',
  acomm int(1) NOT NULL default '0',
  haspoll int(1) NOT NULL default '0',
  pollID int(10) NOT NULL default '0',
  score int(10) NOT NULL default '0',
  ratings int(10) NOT NULL default '0',
  pointswin int(10) NOT NULL default '2',
  pointsloss int(10) NOT NULL default '0',
  pointsdraw int(10) NOT NULL default '1',
  gamesmaxday int(10) NOT NULL default '1',
  declinepoints int(10) NOT NULL default '1',
  active int(11) NOT NULL default '1',
  enabled int(11) NOT NULL default '1',
  challengelimit int(10) NOT NULL default '1',
  challengedays int(10) NOT NULL default '7',
  restrictdates tinyint(1) NOT NULL default '0',
  numdates tinyint(2) NOT NULL default '3',
  timezone varchar(40) NOT NULL default 'GMT',
  restrictmaps tinyint(1) NOT NULL default '0',
  nummaps1 int(10) NOT NULL default '3',
  nummaps2 int(10) NOT NULL default '2',
  standingstype varchar(255) NOT NULL default '',
  maxteams tinyint(40) NOT NULL default '0',
  minplayers int(10) NOT NULL default '0',
  maxplayers int(10) NOT NULL default '500',
  type varchar(255) NOT NULL default 'league',
  expirechalls tinyint(1) NOT NULL default '0',
  expirehours int(10) NOT NULL default '120',
  expirepen int(10) NOT NULL default '1',
  expirebon int(10) NOT NULL default '1',
  whoreports varchar(10) NOT NULL default 'loser',
  mapgroups text NOT NULL,
  PRIMARY KEY  (sid)
) TYPE=MyISAM;

CREATE TABLE nuke_xts_ladderteams (
  ladder_id int(10) NOT NULL default '0',
  team_id int(10) NOT NULL default '0',
  name varchar(40) NOT NULL default '',
  passworddb varchar(10) default NULL,
  mail varchar(50) NOT NULL default '',
  icq varchar(15) NOT NULL default '',
  msn varchar(25) NOT NULL default '',
  country varchar(40) NOT NULL default '',
  games int(10) NOT NULL default '0',
  wins int(10) NOT NULL default '0',
  losses int(10) NOT NULL default '0',
  draws int(10) NOT NULL default '0',
  points int(100) NOT NULL default '0',
  totalwins int(10) NOT NULL default '0',
  totallosses int(10) NOT NULL default '0',
  totaldraws int(10) NOT NULL default '0',
  totalgames int(10) NOT NULL default '0',
  totalpoints int(10) NOT NULL default '0',
  penalties int(10) NOT NULL default '0',
  streakwins int(10) NOT NULL default '0',
  streaklosses int(10) NOT NULL default '0',
  rest int(10) NOT NULL default '0',
  challenged varchar(255) NOT NULL default 'New Team',
  clantags varchar(10) NOT NULL default '',
  homepage varchar(50) NOT NULL default '',
  clanlogo varchar(50) NOT NULL default '',
  ladder_name varchar(255) NOT NULL default 'Not Set',
  challyesno char(3) NOT NULL default 'No',
  rung int(10) NOT NULL default '0',
  FULLTEXT KEY name (name)
) TYPE=MyISAM;

CREATE TABLE nuke_xts_mapgroups (
  id int(255) NOT NULL auto_increment,
  name varchar(100) NOT NULL,
  maps text NOT NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

CREATE TABLE nuke_xts_playedgames (
  game_id int(10) NOT NULL auto_increment,
  winner varchar(40) default NULL,
  loser varchar(40) default NULL,
  date varchar(40) default NULL,
  challenged varchar(255) default NULL,
  challenger varchar(255) default NULL,
  map1 varchar(255) NOT NULL default 'n/a',
  map2 varchar(255) NOT NULL default 'n/a',
  map3 varchar(255) NOT NULL default 'n/a',
  map1t1 varchar(255) NOT NULL default 'n/a',
  map1t2 varchar(255) NOT NULL default 'n/a',
  map2t1 varchar(255) NOT NULL default 'n/a',
  map2t2 varchar(255) NOT NULL default 'n/a',
  map3t1 varchar(255) NOT NULL default 'n/a',
  map3t2 varchar(255) NOT NULL default 'n/a',
  comments varchar(255) NOT NULL default '',
  laddername varchar(255) NOT NULL default 'Error',
  draw tinyint(1) NOT NULL default '0',
  demo varchar(255) NOT NULL default '',
  PRIMARY KEY  (game_id)
) TYPE=MyISAM;


CREATE TABLE nuke_xts_teams (
  team_id int(10) NOT NULL auto_increment,
  name varchar(40) NOT NULL default '',
  passworddb varchar(255) default NULL,
  mail varchar(50) default NULL,
  icq varchar(15) default NULL,
  msn varchar(40) default NULL,
  country varchar(40) default '',
  games int(10) default '0',
  wins int(10) default '0',
  losses int(10) default '0',
  draws int(10) default '0',
  points int(10) default '0',
  totalwins int(10) default '0',
  totallosses int(10) default '0',
  totaldraws int(10) default '0',
  totalpoints int(10) default '0',
  totalgames int(10) default '0',
  penalties int(10) default '0',
  streakwins int(10) default '0',
  streaklosses int(10) default '0',
  playerone varchar(40) NOT NULL default '',
  playerone2 varchar(255) NOT NULL default '',
  clantags varchar(10) NOT NULL default '',
  challenged varchar(10) NOT NULL default 'No',
  homepage varchar(200) NOT NULL default '',
  clanlogo varchar(200) NOT NULL default '',
  ircserver varchar(40) NOT NULL default '''',
  ircchannel varchar(40) NOT NULL default '',
  joinpassword varchar(40) NOT NULL default '',
  recruiting int(1) NOT NULL default '0',
  PRIMARY KEY  (team_id)
) TYPE=MyISAM;


CREATE TABLE nuke_xts_userteams (
  uid int(10) NOT NULL default '0',
  team_id int(10) NOT NULL default '0',
  name varchar(40) NOT NULL default '',
  mail varchar(50) NOT NULL default '',
  icq varchar(15) NOT NULL default '',
  msn varchar(25) NOT NULL default '',
  extra1 varchar(255) NOT NULL default 'none',
  extra2 varchar(255) NOT NULL default 'none',
  extra3 varchar(255) NOT NULL default 'none',
  joindate varchar(10) NOT NULL default 'n/a',
  cocaptain tinyint(1) NOT NULL default '0'
) TYPE=MyISAM;

CREATE TABLE nuke_xts_nukladstaff (
  mod_id int(10) NOT NULL auto_increment,
  mod_name varchar(40) NOT NULL default '',
  mod_games varchar(255) default '0',
  mod_events varchar(255) default '',
  mod_email varchar(255) default '',
  mod_xfire varchar(255) default '',
  mod_msn varchar(255) default '',
  mod_aim varchar(255) default '',
  PRIMARY KEY (mod_id)
) TYPE=MyISAM;