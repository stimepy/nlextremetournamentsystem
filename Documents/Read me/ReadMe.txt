Welcome to NukeLadder Extreme Tournament System (2.6.4) for Nuke Evolution!


BUG REPORTS 
http://www.aodhome.com
and 
stimepy@aodhome.com



Whats New
-------------
see changelog


Notice:
-------------
Things are still underdevelopment but 99% of this should work.  Any Bugs or problems please report them to: http://sourceforge.net/tracker/?func=add&group_id=293105&atid=1238682


Know Problems:
---------------
In the Dragonfly the admin module shows up as $module.  Not able to contact anyone on the dragonflycms site due to cookie troubles (their end, not mine).



New Installation
-----------------
(Note* If you have run Nukeladder previously and want to start fresh, please uninstall as appropriate and remove any Nukeladder related files.)

-Unzip the the package and locate the 'html' folder.
-Upload the contents of the 'html' folder to your root system directory.
-For PHP-Nuke, Locate the Install folder from the zip file, inside you will find options for php and mysql installs.  
We will use the php install.(skip the next step if you use the .sql file)
-Load the install folder to your root nuke directory and run the file in your browser.
-Login to the admin area of your site to enable the module and add any blocks.
-Check the config file for url options, and many other options


Upgrade Installation from Nukeladder 2.5.5x or 2.6.x
-----------------
Nope, not at this time.