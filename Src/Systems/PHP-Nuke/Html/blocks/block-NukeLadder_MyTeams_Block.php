<?php
###############################################################
##X1plugin Competition Management
##Homepage::http://www.projectxnetwork.com
##Copyright:: Shane Andrusiak 2000-2006
##Version 2.5.5
##Updated: 10/25/06 
###############################################################
# My Teams Block - PhpNuke 
###############################################################
# Nuke Globals
global $user, $cookie, $prefix, $db, $sitekey;
#Nuke Cookie Garbage
mt_srand ((double)microtime()*1000000);
$maxran = 1000000;
$random_num = mt_rand(0, $maxran);
$datekey = date("F j");
$rcode = hexdec(md5($_SERVER['HTTP_USER_AGENT'] . $sitekey . $random_num . $datekey));
$code = substr($rcode, 2, 6);
$myteams[] = '';#used to determine if your team is there
cookiedecode($user);
#Start Content
$content ="";
#If a user is logged in
if (is_user($user)) 
{
	#Main Captain Query
	$sql   = "
	SELECT * FROM ".$prefix."_teams
	WHERE playerone='$cookie[1]'";
	$query = $db->sql_query($sql);
	#Loop all rows
	while ($row = $db->sql_fetchrow($query))
	{
		#Add link to content
		$content .="<strong><big>&middot;</big></strong>&nbsp;<a href='modules.php?name=NukeLadder&op=activate_team&t=$row[team_id]'>$row[name]</a><br />";
		#Add team_id to array
		$myteams[] = $row['team_id'];
	}

	#Co-Captain Query
	$sql   = "
	SELECT * FROM ".$prefix."_userteams 
	WHERE uid ='$cookie[0]' 
	AND cocaptain='1';";
	$query = $db->sql_query($sql);
	#Loop all rows
	while ($row2 = $db->sql_fetchrow($query))
	{
		#If not in array of teams 
		if(!in_array($row2['team_id'], $myteams))
		{
			#Team info query
			$loop   = "
			SELECT * FROM ".$prefix."_teams 
			WHERE team_id ='$row2[team_id]';";
			$loopq = $db->sql_query($loop);
			$row  = $db->sql_fetchrow($loopq);
			#Add link to content
			$content .="<strong><big>&middot;</big></strong>&nbsp;<a href='modules.php?name=NukeLadder&op=activate_team&t=$row[team_id]'>$row[name]</a><br />";
			#Add team_id to array
			$myteams[] = $row['team_id'];
		}
	}
	
	#Just a Member  Query
	$sql   = "
	SELECT * FROM ".$prefix."_userteams 
	WHERE uid ='$cookie[0]' 
	AND cocaptain !='1';";
	$query = $db->sql_query($sql);
	#Loop all rows
	while ($row2 = $db->sql_fetchrow($query))
	{
		#If not in array of teams 
		if(!in_array($row2[team_id], $myteams))
		{
			#Team info query
			$loop   = "
			SELECT * FROM ".$prefix."_teams 
			WHERE team_id ='$row2[team_id]';";
			$loopq = $db->sql_query($loop);
		  $row  = $db->sql_fetchrow($loopq);
			#Add link to content
			if(!empty($row['name']))$content .="<strong><big>&middot;</big></strong>&nbsp;<a href='modules.php?name=NukeLadder&op=teamprofile&teamname=$row[name]&panel=roster'>$row[name]</a><br />";
			#Add team_id to array
			$myteams[] = $row['team_id'];
		}
	}
}
else
{
	$content .="<strong><big>&middot;</big></strong>&nbsp;Login to view teams.";
}
if(empty($content))$content ="<strong><big>&middot;</big></strong>&nbsp;You are not a member of a team.";
?>