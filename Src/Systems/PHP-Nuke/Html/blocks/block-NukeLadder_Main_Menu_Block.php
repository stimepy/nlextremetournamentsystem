<?php

if (preg_match( "/block-NukeLadder2Menu.php/", $PHP_SELF)) {
    Header("Location: index.php");
    die();
}
global $prefix, $multilingual, $currentlang, $db, $team, $cookie, $admin;
$module_name="extremetournamentsystem";
if(is_admin($admin))$adminlink = "<strong><big>&middot;</big></strong>&nbsp;<a href=\"modules.php?name=$module_name&amp;op=admin\">Admin Panel</a>";
$content =  "
<strong><big>&middot;</big></strong>&nbsp;<a href=\"modules.php?name=$module_name&amp;op=home\">Events</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"modules.php?name=$module_name&amp;op=myteams\">Team Administration</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"modules.php?name=$module_name&amp;op=createteam\">CreateTeam</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"modules.php?name=$module_name&amp;op=jointeamform\">Join Team</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"modules.php?name=$module_name&amp;op=quitteamform\">Quit Team</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"modules.php?name=$module_name&amp;op=teamlist\">Team List</a><br />
<strong><big>&middot;</big></strong>&nbsp;<a href=\"modules.php?name=$module_name&amp;op=modindex\">Moderator Area</a><br />
$adminlink";
?>
